<?php

//Child Details

class Child{

 public $child_first_name = null;
 public $child_middle_name = null;
 public $child_last_name = null;
 public $child_suffix = null; 
 public $child_birth_time = null;
 public $select = null;
 public $child_birth_date = null;
 public $child_facility_name = null;
 public $child_location_of_birth = null;
 public $child_country_of_birth = null;

 public function __construct($childInformation){  
      $this->child_first_name = $childInformation['child_first_name'];
      $this->child_middle_name = $childInformation['child_middle_name'];
      $this->child_last_name = $childInformation['child_last_name'];
      $this->child_suffix = $childInformation['child_suffix'];
      $this->child_birth_time = $childInformation['child_birth_time'];      
      $this->select = $childInformation['select'];
      $this->child_birth_date = $childInformation['child_birth_date'];      
      $this->child_facility_name = $childInformation['child_facility_name'];
      $this->child_location_of_birth = $childInformation['child_location_of_birth'];
      $this->child_country_of_birth = $childInformation['child_country_of_birth'];

      
    }
    
}

$child = new Child($_POST);

echo "<h2>Child's Details</h2>";
echo "First Name:". $child->child_first_name."<br>";
echo  "Middle Name:".$child->child_middle_name."<br>";
echo  "Last Name:".$child->child_last_name."<br>";
echo  "Suffix:".$child->child_suffix."<br>";
echo  "Time of Birth:".$child->child_birth_time."<br>";
echo  "Sex:".$child->select."<br>";
echo  "Date of birth:".$child->child_birth_date."<br>";
echo  "Facility Name:".$child->child_facility_name."<br>";
echo  "City,Town or Location of Birth:".$child->child_location_of_birth."<br>";
echo  "Country of Birth:".$child->child_country_of_birth."<br>";


   
//Father's details
//FATHER'S CURRENT LEGAL NAME

class Father{

 public $father_first_name = null;
 public $father_middle_name = null;
 public $father_last_name = null;
 public $father_suffix = null; 
 public $fater_date_of_birth = null;
 public $father_birthplace = null;

 public function __construct($fatherInformation){  
    $this->father_first_name = $fatherInformation['father_first_name'];
    $this->father_middle_name = $fatherInformation['father_middle_name'];
    $this->father_last_name = $fatherInformation['father_last_name'];
    $this->father_suffix = $fatherInformation['father_suffix'];
    $this->fater_date_of_birth = $fatherInformation['fater_date_of_birth'];      
    $this->father_birthplace = $fatherInformation['father_birthplace'];
    

    
  }
}
 
$father = new Father($_POST);

echo "<h2>Father's Details</h2>";
echo "<h4>FATHER'S CURRENT LEGAL NAME</h4>";
echo "First Name:". $father->father_first_name."<br>";
echo  "Middle Name:".$father->father_middle_name."<br>";
echo  "Last Name:".$father->father_last_name."<br>";
echo  "Suffix:".$father->father_suffix."<br>";
echo  "Date of Birth:".$father->fater_date_of_birth."<br>";
echo  "Birthplace:".$father->father_birthplace."<br>";


//cirtifer details

// class Certifier{

//     public $certifier_name = null;
//     public $certifier_title = [];
//     public $date_certified = null;
//     public $date_field_register = null; 
//     public $telephone = null;
//     public $ur = null;
//     public $refering_hospital = null;
    
   
//     public function __construct($certifierDeatails){  

//         $this->certifier_name = $certifierDeatails['certifier_name'];
//         $this->certifier_title[] = $certifierDeatails['certifier_title_md'];
//         $this->certifier_title[] = $certifierDeatails['certifier_title_do'];
//         $this->certifier_title[] = $certifierDeatails['certifier_title_hospitai_admin'];
//         $this->certifier_title[] = $certifierDeatails['certifier_title_cnm_cm'];
//         $this->certifier_title[] = $certifierDeatails['certifier_title_other_mid_wife'];
//         $this->certifier_title[] = $certifierDeatails['certifier_title_other_specify'];
//         $this->date_certified = $certifierDeatails['date_certified'];
//         $this->date_field_register = $certifierDeatails['date_field_register'];
//         $this->telephone = $certifierDeatails['telephone'];      
//         $this->ur = $certifierDeatails['ur'];
//         $this->refering_hospital = $certifierDeatails['refering_hospital'];      
        
//     }
      
// }

// $certifier = new Certifier($_POST);

// echo "<h2>Cirtifier's Details</h2>";
// echo "Cirtifier Name:". $certifier->certifier_name."<br>";
// echo  "Title:".implode( "  ,  ", $certifier->certifier_title)."<br>";
// echo  "Date Cirtified:".$certifier->date_certified."<br>";
// echo  "Date Field by Register:".$certifier->date_field_register."<br>";
// echo  "Telephone:".$certifier->telephone."<br>";
// echo  "UR#:".$certifier->ur."<br>";
// echo  "Reffering Hospital:".$certifier->refering_hospital."<br>";


//Mother's Details
//Mother'S CURRENT LEGAL NAME

class Mother{

    public $mother_current_first_name = null;
    public $mother_current_middle_name = null;
    public $mother_current_last_name = null;
    public $mother_current_suffix = null; 
    public $mother_current_date_of_birth = null;
    

    public function __construct($motherInformation){  
        $this->mother_current_first_name = $motherInformation['mother_current_first_name'];
        $this->mother_current_middle_name = $motherInformation['mother_current_middle_name'];
        $this->mother_current_last_name = $motherInformation['mother_current_last_name'];
        $this->mother_current_suffix = $motherInformation['mother_current_suffix'];
        $this->mother_current_date_of_birth = $motherInformation['mother_current_date_of_birth'];      
       
    }
}

$mother = new Mother($_POST);

echo "<h2>Mother's Data</h2>";
echo "<h4>MOTHER'S CURRENT LEGAL NAME</h4>";
echo "First Name:". $mother->mother_current_first_name."<br>";
echo  "Middle Name:".$mother->mother_current_middle_name."<br>";
echo  "Last Name:".$mother->mother_current_last_name."<br>";
echo  "Suffix:".$mother->mother_current_suffix."<br>";
echo  "Birth Time:".$mother->mother_current_date_of_birth."<br>";

