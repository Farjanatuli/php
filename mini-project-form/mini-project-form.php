<?php
if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
    //child'details

     $child_first_name=null;
     $child_middle_name=null;
     $child_last_name=null;
     $child_suffix=null;
     $child_birth_time=null;
     $select=null;
     $child_birth_date=null;
     $child_facility_name=null;
     $child_location_of_birth=null;
     $child_country_of_birth=null;

    if(array_key_exists('child_first_name',$_POST)){
    $child_first_name=$_POST['child_first_name'];
    }
    if(array_key_exists('child_middle_name',$_POST)){
        $child_middle_name=$_POST['child_middle_name'];
    }
    if(array_key_exists('child_last_name',$_POST)){
        $child_last_name=$_POST['child_last_name'];
    }
    if(array_key_exists('child_suffix',$_POST)){
        $child_suffix=$_POST['child_suffix'];
    }
    if(array_key_exists('child_birth_time',$_POST)){
        $child_birth_time=$_POST['child_birth_time'];
    }
    if(array_key_exists('select',$_POST)){
        $select=$_POST['select'];
    }
    if(array_key_exists('child_birth_date',$_POST)){
        $child_birth_date=$_POST['child_birth_date'];
    }
    if(array_key_exists('child_facility_name',$_POST)){
        $child_facility_name=$_POST['child_facility_name'];
    }
    if(array_key_exists('child_location_of_birth',$_POST)){
        $child_location_of_birth=$_POST['child_location_of_birth'];
    }
    if(array_key_exists('child_country_of_birth',$_POST)){
        $child_country_of_birth=$_POST['child_country_of_birth'];
    }
    
    



    //Father's details

     //FATHER'S CURRENT LEGAL NAME
     $father_first_name=null;
     $father_middle_name=null;
     $father_last_name=null;
     $father_suffix=null;
     $fater_date_of_birth=null;
     $father_birthplace=null;

    if(array_key_exists('father_first_name',$_POST)){
    $father_first_name=$_POST['father_first_name'];
    }
    if(array_key_exists('father_middle_name',$_POST)){
        $father_middle_name=$_POST['father_middle_name'];
    }
    if(array_key_exists('father_last_name',$_POST)){
        $father_last_name=$_POST['father_last_name'];
    }
    if(array_key_exists('father_suffix',$_POST)){
        $father_suffix=$_POST['father_suffix'];
    }
    if(array_key_exists('fater_date_of_birth',$_POST)){
        $fater_date_of_birth=$_POST['fater_date_of_birth'];
    }
    if(array_key_exists('father_birthplace',$_POST)){
        $father_birthplace=$_POST['father_birthplace'];
    }
    


    //cirtifer details
    $certifier_name=null;
    $cirtifier_title=[];
    $date_certified=null;
    $date_field_register=null;
    $telephone=null;
    $ur=null;
    $refering_hospital=null;


       if(array_key_exists('certifier_name',$_POST)){
        $certifier_name=$_POST['certifier_name'];
        }
        if(array_key_exists('certifier_title_md',$_POST)){
            $cirtifier_title[]=$_POST['certifier_title_md'];
        }
        if(array_key_exists('certifier_title_do',$_POST)){
            $cirtifier_title[]=$_POST['certifier_title_do'];
        }
        if(array_key_exists('certifier_title_hospitai_admin',$_POST)){
            $cirtifier_title[]=$_POST['certifier_title_hospitai_admin'];
        }
        if(array_key_exists('certifier_title_cnm_cm',$_POST)){
            $cirtifier_title[]=$_POST['certifier_title_cnm_cm'];
        }
        if(array_key_exists('certifier_title_other_mid_wife',$_POST)){
            $cirtifier_title[]=$_POST['certifier_title_other_mid_wife'];
        }
        if(array_key_exists('certifier_title_other_specify',$_POST)){
            $cirtifier_title[]=$_POST['certifier_title_other_specify'];
        }
        if(array_key_exists('date_certified',$_POST)){
            $date_certified=$_POST['date_certified'];
        }
        if(array_key_exists('date_field_register',$_POST)){
            $date_field_register=$_POST['date_field_register'];
        }
        if(array_key_exists('telephone',$_POST)){
            $telephone=$_POST['telephone'];
        }

        if(array_key_exists('ur',$_POST)){
            $ur=$_POST['ur'];
        }
        if(array_key_exists('refering_hospital',$_POST)){
            $refering_hospital=$_POST['refering_hospital'];
        }
        
        


     //mother's details
     

     //Mother'S CURRENT LEGAL NAME
      $mother_current_first_name=null;
      $mother_current_middle_name=null;
      $mother_current_last_name=null;
      $mother_current_suffix=null;
      $mother_current_date_of_birth=null;

      

     if(array_key_exists('mother_current_first_name',$_POST)){
        $mother_current_first_name=$_POST['mother_current_first_name'];
     }
    if(array_key_exists('mother_current_middle_name',$_POST)){
        $mother_current_middle_name=$_POST['mother_current_middle_name'];
    }
    if(array_key_exists('mother_current_last_name',$_POST)){
        $mother_current_last_name=$_POST['mother_current_last_name'];
    }
    if(array_key_exists('mother_current_suffix',$_POST)){
        $mother_current_suffix=$_POST['mother_current_suffix'];
    }
    if(array_key_exists('mother_current_date_of_birth',$_POST)){
        $mother_current_date_of_birth=$_POST['mother_current_date_of_birth'];
    }


    //Mother's Name Prior to first Marriage
      $mother_first_marriage_first_name=null;
      $mother_first_marriage_middle_name=null;
      $mother_first_marriage_last_name=null;
      $mother_first_marriage_suffix=null;
      $mother_Birthplace=null;
      $mother_state=null;
      $mother_country=null;
      $mother_City_Town_or_Location=null;
      $mother_street_number=null;
      $mother_apartment_no=null;
      $zip_code=null;
      $inside_city_limits=null;
  

    if(array_key_exists('mother_first_marriage_first_name',$_POST)){
        $mother_first_marriage_first_name=$_POST['mother_first_marriage_first_name'];
    }
    if(array_key_exists('mother_first_marriage_middle_name',$_POST)){
        $mother_first_marriage_middle_name=$_POST['mother_first_marriage_middle_name'];
    }
    if(array_key_exists('mother_first_marriage_last_name',$_POST)){
        $mother_first_marriage_last_name=$_POST['mother_first_marriage_last_name'];
    }
    if(array_key_exists('mother_first_marriage_suffix',$_POST)){
        $mother_first_marriage_suffix=$_POST['mother_first_marriage_suffix'];
    }
    if(array_key_exists('mother_Birthplace',$_POST)){
        $mother_Birthplace=$_POST['mother_Birthplace'];
    }
    if(array_key_exists('mother_state',$_POST)){
        $mother_state=$_POST['mother_state'];
    }
    if(array_key_exists('mother_country',$_POST)){
        $mother_country=$_POST['mother_country'];
    }
    if(array_key_exists('mother_City_Town_or_Location',$_POST)){
        $mother_City_Town_or_Location=$_POST['mother_City_Town_or_Location'];
    }
    if(array_key_exists('mother_street_number',$_POST)){
        $mother_street_number=$_POST['mother_street_number'];
    }
    if(array_key_exists('mother_apartment_no',$_POST)){
        $mother_apartment_no=$_POST['mother_apartment_no'];
    }
     if(array_key_exists('zip_code',$_POST)){
         $zip_code=$_POST['zip_code'];
    }
    
     if(array_key_exists('inside_city_limits',$_POST)){
        $inside_city_limits=$_POST['inside_city_limits'];
    }

    



    if(array_key_exists('submit',$_POST)){
    
        
        echo "<h2>Child's Details</h2>";
        echo "First Name:<b> $child_first_name </b>  <br>";
        echo "Middle Name:<b> $child_middle_name </b>  <br>";
        echo "Last Name:<b>  $child_last_name</b> <br>";
        echo "Suffix:<b>  $child_suffix  </b> <br>";
        echo "Time of Birth:<b> $child_birth_time </b>  <br>";
        echo "Sex:<b> $select </b>  <br>";
        echo "Date of birth:<b> $child_birth_date </b> <br>";
        echo "Facility Name:<b> $child_facility_name</b> <br>";
        echo "City,Town or Location of Birth:<b>  $child_location_of_birth </b> <br>";
        echo "Country of Birth:<b> $child_country_of_birth </b>  <br>";
    
        
    
        echo "<h2>Father's Details</h2>";
        echo "<h4>FATHER'S CURRENT LEGAL NAME</h4>";
        echo "First Name:<b> $father_first_name </b>  <br>";
        echo "Middle Name:<b> $father_middle_name </b>  <br>";
        echo "Last Name:<b>  $father_last_name</b> <br>";
        echo "Suffix:<b>  $father_suffix  </b> <br>";
        echo "Date of Birth:<b> $fater_date_of_birth </b>  <br>";
        echo "Birthplace:<b> $father_birthplace </b>  <br>";
    

    
        echo "<h2>Cirtifier's Details</h2>";
        echo "Cirtifier Name:<b> $certifier_name </b>  <br>";
        echo "Title:<b>".implode( "  ,  ", $cirtifier_title)."</b><br>";
        echo "Date Cirtified:<b>  $date_certified</b> <br>";
        echo "Date Field by Register:<b>  $date_field_register  </b> <br>";
        echo "Telephone:<b> $telephone </b>  <br>";
        echo "UR#:<b> $ur </b>  <br>";
        echo "Reffering Hospital:<b> $refering_hospital </b>  <br>";
    
    
     
        echo "<h2>Mother's Data</h2>";
        echo "<h4>MOTHER'S CURRENT LEGAL NAME</h4>";
        echo "First Name:<b> $mother_current_first_name </b>  <br>";
        echo "Middle Name:<b> $mother_current_middle_name </b>  <br>";
        echo "Last Name:<b>  $mother_current_last_name</b> <br>";
        echo "Suffix:<b>  $mother_current_suffix  </b> <br>";
        echo "Birth Time:<b> $mother_current_date_of_birth </b>  <br>";



        echo "<h4>Mother's Name Prior To First Marriage</h4>";
        echo "First Name:<b> $mother_first_marriage_first_name </b>  <br>";
        echo "Middle Name:<b> $mother_first_marriage_middle_name </b>  <br>";
        echo "Last Name:<b>  $mother_first_marriage_last_name</b> <br>";
        echo "Suffix:<b>  $mother_first_marriage_suffix  </b> <br>";
        echo "Birth Place:<b> $mother_Birthplace </b>  <br>";
        echo "Residance of Mother-state:<b> $mother_state </b>  <br>";
        echo "Country:<b> $mother_country </b>  <br>";
        echo "City,Town or Location:<b> $mother_City_Town_or_Location </b>  <br>";
        echo "Street and Number:<b> $mother_street_number </b> <br>";
        echo "Apartment No:<b> $mother_apartment_no</b> <br>";
        echo "Zip Code:<b>  $zip_code </b> <br>";
        echo "Info About City Limit:<b> $inside_city_limits</b> <br>";
       
    
    }
    
    
//cirtifer details

class Certifier{

    public $certifier_name = null;
    public $certifier_title = [];
    public $date_certified = null;
    public $date_field_register = null; 
    public $telephone = null;
    public $ur = null;
    public $refering_hospital = null;
    
   
    public function __construct($certifierDeatails){  
        $this->certifier_name = $certifierDeatails['certifier_name'];
        $this->certifier_title[] = $certifierDeatails['certifier_title_md'];
        $this->certifier_title[] = $certifierDeatails['certifier_title_do'];
        $this->certifier_title[] = $certifierDeatails['certifier_title_hospitai_admin'];
        $this->certifier_title[] = $certifierDeatails['certifier_title_cnm_cm'];
        $this->certifier_title[] = $certifierDeatails['certifier_title_other_mid_wife'];
        $this->certifier_title[] = $certifierDeatails['certifier_title_other_specify'];

        $this->date_certified = $certifierDeatails['date_certified'];
        $this->date_field_register = $certifierDeatails['date_field_register'];
        $this->telephone = $certifierDeatails['telephone'];      
        $this->ur = $certifierDeatails['ur'];
        $this->refering_hospital = $certifierDeatails['refering_hospital'];      
        
    }
      
}
$certifier = new Certifier($_POST);

echo "<h2>Cirtifier's Details</h2>";
echo "Cirtifier Name:". $certifier->certifier_name."<br>";
echo  "Title:".implode( "  ,  ", $certifier->certifier_title)."<br>";
echo  "Date Cirtified:".$certifier->date_certified."<br>";
echo  "Date Field by Register:".$certifier->date_field_register."<br>";
echo  "Telephone:".$certifier->telephone."<br>";
echo  "UR#:".$certifier->ur."<br>";
echo  "Reffering Hospital:".$certifier->refering_hospital."<br>";


}