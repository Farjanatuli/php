<?php

$a = 3;
echo ++$a;//Pre-increment-Increments $a by one, then returns $a
echo '<br>';

$a = 3;
echo $a++;//Post-increment->Returns $a, then increments $a by one.
echo '<br>';
echo $a;
echo '<br>';

$a = 3;
echo --$a;//Pre-decrement->Decrements $a by one, then returns $a.
echo '<br>';

$a = 3;
echo $a--;//Post-decrement->Returns $a, then decrements $a by one.
echo '<br>';
echo $a;
echo '<br>';
