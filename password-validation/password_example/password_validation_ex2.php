<?php


if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){//post Post POST  $text='abcD; strtoupper($text)   ABCD
   $password=null;
    if(array_key_exists('userPassword',$_POST)){
      
        $password=$_POST['userPassword'];
    }
    $errors= errorValidation($password);
    if(count($errors)>0){
    displayError($errors);
    echo $password;
    }

}


function checkUppercase($password){
    $uppercase=preg_match('@[A-Z]@', $password);
    if($uppercase){
        return true;
    }
    return false;
}

function checkLowercase($password){
    $lowercase=preg_match('@[a-z]@', $password);
    if($lowercase){
        return true;
    }
    return false;
}
 

function checkNumber($password){
    $number=preg_match('@[0-9]@', $password);
    if($number){
        return true;
    }
    return false;
}

function checkSpecialCharacter($password){
    $specialCharacter=preg_match('@[^\w]@', $password);
    if($specialCharacter){
        return true;
    }
    return false;
}



function checkSpace($password){
   
    for($i=0;$i<strlen($password);$i++){
        if($password[$i]==" "){
            return false;
        }
        return true;

    }
}

function checkLength($password){
    $length=strlen($password);
    if($length>=8){
        return true;
    }
    return false;
}

function displayError($errors){
    echo "<ul>";
    foreach($errors as $error){ 
        echo "<li><b>$error</b></li>"; 
    }
    echo "</ul>";
}




function errorValidation($password){
    $errors=[];
    if(!checkUppercase($password)){
        $errors[]="password must contain at least one uppercase letter (A-Z)!";
    }
    if(!checkLowercase($password)){
        $errors[]="password must contain at least one lowercase letter (a-z)!";
    }
    if(!checkNumber($password)){
        $errors[]="password must contain at least one number (0-9)!";
    }
    if(!checkSpecialCharacter($password)){
        $errors[]="password must contain at least one special character (@!#)!";
    }

    if(!checkSpace($password)){
        $errors[]="password must be space free, please remove white space.";
    }
    if(!checkLength($password)){
        $errors[]="password length  at least 8 possible combination";
    }
    return $errors;
}




