<?php

//1.string-functions:

//a.echo, b.explode, c.implode, d.strlen, e.strpos, f.strtolower, 
//g.strtoupper, h.substr, i.md5, j.nl2br, k.str_repeat, l.str_replace, m.trim, n.ucfirst

//example strlen
$x="I am Tuli";
echo strlen($x);//length
echo "<br>";
echo "<br>";

//example str_word_count
$x="I am Tuli";
echo str_word_count($x);//word count
echo "<br>";
echo "<br>";

//example strrev
$x="I am Tuli";
echo strrev($x);//reverse
echo "<br>";
echo "<br>";

//example strpos
$x="I am Tuli";
echo strpos($x,"Tuli");//position
echo "<br>";
echo "<br>";

//example str_replace
$x="I am Tuli";
echo str_replace("Tuli","Tanvir",$x);//replace
echo "<br>"."<br>";


//example strtolower
$str = "I Am Tuli";
$str = strtolower($str);
echo $str; //lower
echo "<br>"."<br>";

//example strtoupper
$str = "Mary Had A Little Lamb and She LOVED It So";
$str = strtoupper($str);
echo $str;                    //upper
echo "<br>"."<br>";

//example substr
$substr = "pondit";
echo substr($substr,0,5);//substr
echo "<br>"."<br>";


// example1 nl2br
$string = "This\r\nis\n\ra\nstring\r";
echo nl2br($string);
echo "<br>"."<br>";

//example2 n12br
function nl2br2($string) {
    $string = str_replace(array("\r\n", "\r", "\n"), "<br />", $string);
    return $string;
    }
    echo "<br>"."<br>";

//example trim   
$str = "Hello World!";
echo $str . "<br>";
echo trim($str,"Hed!");
echo "<br>"."<br>";

//example explode
$x= "hello";
$y = "hello,there";
$z = ',';
var_dump( explode( ',', $x ) );
var_dump( explode( ',', $y ) );
var_dump( explode( ',', $z ) );//explode
echo "<br>"."<br>";

//example implode
$student_batch1 = array("Tuli","Tanvir","Rikta","Oishe");
$student_batch2 = array("Jannat","Abdur Rahim");   
echo "Student batch1 is: '".implode("','",$student_batch1)."'<br>";
echo "Student batch2 is: '".implode("','",$student_batch2)."'<br>";//implode
echo "<br>"."<br>";

//example string fungtion
$x = "Pondit world!";
$y = 'Career guaranteed course!';
echo $x;
echo "<br>";
echo $y;
echo "<br>"."<br>";

//example string function
$a = "Error";
$b = $a . "connection!"; 
echo $b ."<br>";

$a = "Hello";
$a .= "World!"; 
echo $a;
echo "<br>"."<br>";

//example1 ucfirst
$foo = 'I am pondit student!';
$foo = ucfirst($foo);             
echo $foo;
echo "<br>"."<br>";

//example2 ucfirst
$bar = 'I AM PONDIT STUDENT!';
$bar = ucfirst($bar);             
$bar = ucfirst(strtolower($bar)); 
echo $bar;
echo "<br>"."<br>";


//example 1 md5
$str = "Hello";
echo md5($str);

if (md5($str) == "8b1a9953c4611296a827abf8c47804d7")
  {
  echo "<br>Hello world!";
  exit;
  }
  echo "<br>"."<br>";



//example2 md5
$str = "Hello";
echo "The string: ".$str."<br>";
echo "TRUE - Raw 16 character binary format: ".md5($str, TRUE)."<br>";
echo "FALSE - 32 character hex number: ".md5($str)."<br>";
echo "<br>"; 


?>