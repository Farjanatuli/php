<?php
$a = 32;
echo "a is " . is_int($a) . "<br>";

$b = 0;
echo "b is " . is_int($b) . "<br>";

$c = 32.5;
echo "c is " . is_int($c) . "<br>";

$d = "32";
echo "d is " . is_int($d) . "<br>";

$e = true;
echo "e is " . is_int($e) . "<br>";

$f = "null";
echo "f is " . is_int($f) . "<br>";
?>
