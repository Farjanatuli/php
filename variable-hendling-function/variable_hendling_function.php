<?php

//1.variable-handling-functions:

//a.empty, b.is_array, c.is_null, d.isset, e.print_r, f.serialize, 
//g.var_dump, h.array_key_exists, i.unset, j.gettype, k.is_int


//example
$a = 0;

// True because $a is empty
if (empty($a)) {
  echo "Variable 'a' is empty.<br>";
}

// True because $a is set
if (isset($a)) {
  echo "Variable 'a' is set";
}
?>
